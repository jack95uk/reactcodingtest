import React from 'react';
import {render} from 'react-dom';
import Spinner from './Spinner';

class FruitMachine extends React.Component {


  getRandomValue(number) {
    if (typeof number != "number" || number < 1) return null;
    return Math.floor(Math.random() * number);
  }

spin() {
  console.log('Spin');
}

  render() {
    const colours = Object.keys(this.props.state.colors);

    var boxes = [];
    //3 boxes
    for (var i = 0; i < 3; i++) {
      //4 colours
      boxes.push(this.getRandomValue(4));
    }

    if (boxes.every(box => box === boxes[0])) {
      alert('WINNER')
    }

    return (
      <div className="fruitMachine">
        <div className="spinners">
          <Spinner bgColour={colours[boxes[0]]}/>
          <Spinner bgColour={colours[boxes[1]]}/>
          <Spinner bgColour={colours[boxes[2]]}/>

        </div>
        <div className="buttonRow">
          <button className="spinButton" onClick={this.spin} disabled>SPIN</button>
          <p>hit f5 to play</p>
        </div>
      </div>
    )
  }
}

export default FruitMachine;

import React, { Component } from 'react';
import IconImage from 'images/icon.png';
import FruitMachine from './FruitMachine';


class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      colors : {
        red: '#ff0000',
        green: '#00ff00',
        blue: '#0000ff',
        yellow: '#ffff00'
      }
    }

  }


  render() {
    return (
      <div className="shell">
        <div className='hero'>
          <h1>FRUIT MACHINE</h1>
        </div>
        <FruitMachine state={this.state} boxes={3}/>
      </div>
    )
  }
}

export default App

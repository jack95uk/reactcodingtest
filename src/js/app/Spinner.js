import React from 'react';

const Spinner = (props) => {
    return (
      <div className="spinner" style={{backgroundColor:props.bgColour}}></div>
    )
}

export default Spinner;
